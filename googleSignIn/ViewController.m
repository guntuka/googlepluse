//
//  ViewController.m
//  googleSignIn
//
//  Created by Ramoji Krian on 18/04/2560 BE.
//  Copyright © 2560 BE Ramoji Krian. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()
{
    AppDelegate * appdel;
}

@property NSMutableDictionary * dict;
@property UIActivityIndicatorView * myActivityIndicator;

@end

@implementation ViewController
@synthesize dict,nameTF,emailTF,gmilIDno;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    // sumanchanges......
    

    [GIDSignIn sharedInstance].uiDelegate = self;

}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
   
    [_myActivityIndicator stopAnimating];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (IBAction)didTapSignOut:(id)sender {
//    [[GIDSignIn sharedInstance] signOut];
//}

- (IBAction)signInBtn:(id)sender {
    [[GIDSignIn sharedInstance] signIn];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"fullName"];
    nameTF.text=savedValue;
    NSString *savedValue1 = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"email"];
    emailTF.text=savedValue1;
    NSString *savedValue2 = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"userId"];
    gmilIDno.text=savedValue2;
    
    [[GIDSignIn sharedInstance] signOut];
 }
- (IBAction)logOut:(id)sender {

//    [[GIDSignIn sharedInstance] signOut];
//    nameTF.text=nil;
//    emailTF.text=nil;
//    gmilIDno.text=nil;
    
}


@end
