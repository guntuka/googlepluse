//
//  main.m
//  googleSignIn
//
//  Created by Ramoji Krian on 18/04/2560 BE.
//  Copyright © 2560 BE Ramoji Krian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
